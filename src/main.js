import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import vuetify from './plugins/vuetify';
import VueCarousel from 'vue-carousel';
import VueMeta from 'vue-meta';

import store from './store';
import VeeValidate from 'vee-validate';
import ApiService from './api/api.service';

Vue.use(VeeValidate);
ApiService.init();
Vue.use(VueMeta, {
  // optional pluginOptions
  refreshOnceOnNavigation: true
})
Vue.use(VueCarousel);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
