import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import {CHECK_AUTH} from "../store/actions.type";

import Splash from '../views/Onboarding/Splash.vue'
import Onboarding from '../views/Onboarding/Onboarding.vue'
import Register from '../views/Onboarding/Register.vue'
import Login from '../views/Onboarding/Login.vue'
import SetUpPassword from '../views/Onboarding/SetUpPassword.vue'
import Welcome from '../views/Onboarding/Welcome.vue'
import Step1 from '../views/CreateProfile/Step1.vue'
import Home from '../views/Activity/Home.vue'
import Compose from '../views/Activity/Compose.vue'
import FriendRequest from '../views/Activity/FriendRequest.vue'
import FriendList from '../views/Activity/FriendList.vue'
import HomeFriend from '../views/Activity/HomeFriend.vue'
import Settings from '../views/Settings/Settings.vue'
import ChangePassword from '../views/Settings/ChangePassword.vue'
import ChangeEmail from '../views/Settings/ChangeEmail.vue'
import DeleteAccount from '../views/Settings/DeleteAccount.vue'
import CreateEvent from '../views/Event/CreateEvent.vue'
import MyEvent from '../views/Event/MyEvent.vue'
import InviteFriend from '../views/Event/InviteFriend.vue'
import PublicEvent from '../views/Event/PublicEvent.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'splash',
    component: Splash,
    meta: {
        requiresAuth: true,
        hideForAuth: true,
    }
  },
  {
    path: '/home',
    name: 'home',
    component: Home,
    meta: {
        requiresAuth: true,
        hideForAuth: false,
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
        requiresAuth: false,
        hideForAuth: false,
    }
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/onboarding',
    name: '/onboarding',
    component: Onboarding
  },
  {
    path: '/set-up-password',
    name: 'set-up-password',
    component: SetUpPassword
  },
  {
    path: '/welcome',
    name: 'welcome',
    component: Welcome
  },
  {
    path: '/create-profile',
    name: 'step1',
    component: Step1
  },
  {
    path: '/compose',
    name: 'compose',
    component: Compose
  },
  {
    path: '/friend-request',
    name: 'friend-request',
    component: FriendRequest
  },
  {
    path: '/friend-list',
    name: 'friend-list',
    component: FriendList
  },
  {
    path: '/home-friend',
    name: 'home-friend',
    component: HomeFriend
  },
  {
    path: '/settings',
    name: 'settings',
    component: Settings
  },
  {
    path: '/change-password',
    name: 'change-password',
    component: ChangePassword
  },
  {
    path: '/change-email',
    name: 'change-email',
    component: ChangeEmail
  },
  {
    path: '/delete-account',
    name: 'delete-account',
    component: DeleteAccount
  },
  {
    path: '/create-event',
    name: 'create-event',
    component: CreateEvent
  },
  {
    path: '/my-event',
    name: 'my-event',
    component: MyEvent
  },
  {
    path: '/invite-friend',
    name: 'invite-friend',
    component: InviteFriend
  },
  {
    path: '/public-event',
    name: 'public-event',
    component: PublicEvent
  },
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if(to.meta.requiresAuth && !store.getters.isAuthenticated){
      next('/login');
  } else if(to.meta.hideForAuth && store.getters.isAuthenticated){
      next('/home');
  } else {
      next();
  }
});



export default router
